<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->engine='InnoDB ROW_FORMAT=DYNAMIC';
            $table->bigIncrements('id');
            $table->string('name')->comment('Название организации');
            $table->string('inflexive_name')->nullable()->comment('Склоняемое имя организации');
            $table->string('slug')->nullable()->comment('ЧПУ');
            $table->string('city')->nullable()->comment('Город');
            $table->string('city_kaz')->nullable()->comment('Город на казахском языке');
            $table->string('address_kaz')->nullable()->comment('Адрес на казахском языке');
            $table->string('address')->nullable()->comment('Адрес организации');
            $table->string('email')->nullable()->comment('Электронная почта организации');
            $table->string('phone')->nullable()->comment('Сотовый организации');
            $table->string('mobile_phone')->comment('Рабочий телефон');
            $table->text('bank_requisites')->nullable()->comment('Банковские реквизиты организации');
            $table->string('mrp')->nullable()->comment('МРП');
            $table->string('license_number')->comment('Номер лицензии');
            $table->string('license_date')->comment('Дата лицензии');
            $table->string('license_creator')->comment('Кем выдана лицензия')->nullable();
            $table->string('owner_fullname')->comment('ФИО ЧСИ');
            $table->string('owner_id')->comment('ИИН ЧСИ');
            $table->string('agency_request_for')->nullable()->comment('Орган, откуда запрашиваешь правдоки');
            $table->string('agency_for_arrest_car')->comment('Орган, куда отправляют арест на авто');
            $table->string('agency_for_arrest_spec_car')->comment('Орган, куда отправляют арест на спецтехнику');
            $table->string('agency_address_for_request_passport')->comment('Адрес, откуда запрашиваешь технический паспорт');
            $table->string('agency_address_for_request_gov_act')->comment('Орган, откуда запрашиваешь гос.акт');
            $table->string('seller_company_name')->comment('Наименование органа, который проводит торги.')->nullable();
            $table->string('seller_site')->comment('Сайт электронной торговой площадки')->nullable();
            $table->string('address_agency_request_for')->nullable()->comment('Адрес органа, откуда запрашиваешь правдоки');
            $table->string('agency_request_for_tech_id')->nullable()->comment('Орган откуда запрашиваешь технический паспорт и гос.акт');
            $table->string('address_agency_request_for_tech_id')->nullable()->comment('Адрес органа откуда запрашиваешь технический паспорт и гос.акт');
            $table->string('agency_request_for_deposit')->nullable()->comment('Орган, откуда заправшиваешь договор залога');
            $table->string('address_agency_request_for_deposit')->nullable()->comment('Адрес органа, откуда заправшиваешь договор залога');
            $table->string('agency_request_for_arrest_copy')->nullable()->comment('Орган, откуда запрашиваешь копии постановлении о наложении ареста');
            $table->string('address_agency_request_for_arrest_copy')->nullable()->comment('/');

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
