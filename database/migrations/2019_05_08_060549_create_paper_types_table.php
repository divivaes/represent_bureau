<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper_types', function (Blueprint $table) {
            $table->engine='InnoDB ROW_FORMAT=DYNAMIC';
            $table->bigIncrements('id');

            $table->string('paper_name')->comment('Наименование документа');
            $table->string('inflexive_paper_name')->nullable()->comment('Склонямое наименование документа');
            $table->string('paper_tpl_path')->nullable()->comment('Путь к документу');
            $table->string('status')->default(0)->comment('Статус документа 0=вне использовании, 1=активен');
            $table->integer('index')->default(1000)->comment('Порядок шаблона по листу');
            $table->string('dependencies', 255)->nullable()->comment('ID шаблона от которых зависит данный шаблон');
            $table->string('injections', 255)->nullable()->comment('ID шаблона которые заполняются сразу');

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper_types');
    }
}
