<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperTypeAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper_type_attributes', function (Blueprint $table) {
            $table->engine='InnoDB ROW_FORMAT=DYNAMIC';
            $table->bigIncrements('id');

            $table->unsignedBigInteger('paper_type_id')->nullable();
            $table->foreign('paper_type_id')
                ->references('id')
                ->on('paper_types')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->string('attr_name')->nullable()->comment('Наименование атрибута для документа');
            $table->text('attr_usage')->nullable()->comment('Название атрибута для использования на сервере');
            $table->string('attr_type')->nullable()->comment('Тип данных [string, date, integer etc]');
            $table->string('attr_format')->nullable()->comment('Опциональное поле, если тип данных потребует определенного формата');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper_type_attributes');
    }
}
