<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('papers', function (Blueprint $table) {
            $table->engine='InnoDB ROW_FORMAT=DYNAMIC';
            $table->bigIncrements('id');

            $table->bigInteger('user_id', false, true)->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');
            $table->bigInteger('organization_id', false, true)->nullable();
            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');

            $table->string('claimant_type')->nullable()->comment('Тип взыскателя');
            $table->string('debtor_type')->nullable()->comment('Тип должника');
            // Поле для заполнения про должника
            $table->string('debtor_company_name')->comment('Название ИП или ТОО должника')->nullable();
            $table->string('debtor_firstname')->nullable()->comment('Имя должника');
            $table->string('debtor_lastname')->nullable()->comment('Фамилия должника');
            $table->string('debtor_surname')->nullable()->comment('Отчество должника');
            $table->json('common_constitutor')->nullable()->comment('Учредитель');
            $table->json('common_constitutor_address')->nullable()->comment('Адрес учредителя');
            $table->string('debtor_mentor_firstname')->nullable()->comment('Имя руководителя должника');
            $table->string('debtor_mentor_lastname')->nullable()->comment('Фамилия руководителя должника');
            $table->string('debtor_mentor_surname')->nullable()->comment('Отчество руководителя должника');
            $table->string('debtor_mentor_address')->nullable()->comment('Адрес руководителя должника');
            $table->string('debtor_email')->nullable()->comment('Электронная почта должника');
            $table->string('debtor_phone')->nullable()->comment('Телефон должника');
            $table->string('debtor_id')->nullable()->comment('ИИН или БИН должника');
            $table->string('debtor_address')->nullable()->comment('Адрес должника по месту регистрации');
            $table->string('debtor_perflist_address')->nullable()->comment('Адрес должника по исполнительному листу');
            $table->string('debtor_workplace')->nullable()->comment('Наименование организации, где работает должник');
            $table->string('debtor_workplace_address')->nullable()->comment('Адрес организации, где работает должник');
            // Поле для заполнения про взыскателя
            $table->string('claimant_company_name')->nullable()->comment('Название ИП или ТОО взыскателя');
            $table->string('claimant_firstname')->nullable()->comment('Имя взыскателя');
            $table->string('claimant_lastname')->nullable()->comment('Фамилия взыскателя');
            $table->string('claimant_surname')->nullable()->comment('Отчество взыскателя');
            $table->string('claimant_id')->nullable()->comment('ИИН или БИН взыскателя');
            $table->string('claimant_mentor_firstname')->nullable()->comment('Имя руководителя взыскателя');
            $table->string('claimant_mentor_lastname')->nullable()->comment('Фамилия руководителя взыскателя');
            $table->string('claimant_mentor_surname')->nullable()->comment('Отчество руководителя взыскателя');
            $table->string('claimant_mentor_address')->nullable()->comment('Адрес руководителя взыскателя');
            $table->string('claimant_email')->nullable()->comment('Электронная почта взыскателя');
            $table->string('claimant_phone')->nullable()->comment('Номер телефона взыскателя');
            $table->string('claimant_address')->nullable()->comment('Адрес взыскателя');
            $table->string('claimant_sum')->nullable()->comment('Сумма взыскания всего');
            $table->string('claimant_content')->nullable()->comment('Сущность взыскания');
            // Поле для заполнения исполнительного документа
            $table->string('enf_proc_number')->nullable()->comment('Номер исполнительного производства');
            $table->string('enf_proc_date')->nullable()->comment('Дата возбуждения исполнительного производства');
            $table->string('exec_doc_name')->comment('Наименование исполнительного документа');
            $table->string('exec_doc_number')->comment('Номер исполнительного документа');
            $table->string('exec_doc_owner')->comment('Kем выдан исполнительный документ');
            $table->string('exec_doc_created_at')->comment('Дата выдачи исполнительного документа');

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('papers');
    }
}
