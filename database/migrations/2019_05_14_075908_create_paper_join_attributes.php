<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperJoinAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper_join_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('paper_join_id');
            $table->foreign('paper_join_id')
                ->references('id')->on('paper_joins')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('paper_type_attribute_id');
            $table->foreign('paper_type_attribute_id')
                ->references('id')
                ->on('paper_type_attributes')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->text('attribute_value')->nullable();

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper_join_attributes');
    }
}
