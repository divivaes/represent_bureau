<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';
            $table->bigIncrements('id');

            $table->bigInteger('organization_id', false, true)->nullable();
            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');

            $table->string('role')->nullable();
            $table->string('fullname')->nullable()->comment('ФИО');
            $table->string('login')->unique()->nullable()->comment('Логин для входа');
            $table->string('email')->unique()->nullable()->comment('Почта');
            $table->string('phone')->unique()->nullable()->comment('Телефон');
            $table->timestamp('email_verified_at')->nullable()->comment('Время подтверждения');
            $table->string('password')->comment('Пароль');
            $table->boolean('status')->default(1)->comment('Статус пользователя 1-активен, 2-забанен');
            $table->string('session_id')->nullable();

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
