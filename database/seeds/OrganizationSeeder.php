<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{
    public function run()
    {
        DB::table('organizations')->insert([
            'name' => 'ИП Шерматова',
            'inflexive_name' => 'Шерматов С.М.',
            'slug' => Str::slug('Шерматов С.М.'),
            'city' => 'г. Алматы',
            'city_kaz' => 'Алматы к.',
            'address' => 'г.Алматы, ул.Байзакова д.155 кв.33',
            'address_kaz' => 'Алматы қ. Байзақов к. 155 үй 33',
            'email' => 'Sake_007@mail.ru',
            'phone' => '8(727)225-77-75',
            'mobile_phone' => '8 707 341 66 72',
            'license_number' => '1008',
            'license_date' => '19.02.2014',
            'license_creator' => 'КИСА МЮ РК',
            'owner_fullname' => 'ШЕРМАТОВ САКЕН МУХТАРОВИЧ',
            'owner_id' => '810710301087',
            'bank_requisites' => 'текуший счёт (ЧСИ) частного судебного исполнителя Шерматова Сакена Мухтаровича (ИИН 810710301087), КБЕ 19, № KZ 138 562 204 105 481 872 (KZT), в АГФ  АО «Банк ЦентрКредит» г.Алматы, БИК банка KCJBKZKX',
            'mrp' => '2525',
            'agency_for_arrest_car' => 'УАП УВД г. Алматы',
            'seller_company_name' => 'РПЧСИ «Единая электронная торговая площадка»',
            'seller_site' => 'https://etp.adilet.gov.kz',
            'agency_for_arrest_spec_car' => 'КГУ «Управление предпринимательства и инвестиции г.Алматы»',
            'agency_address_for_request_passport' => 'г.Алматы, пр.Абая д.111',
            'agency_address_for_request_gov_act' => 'НАО ГК «Правительство для граждан» по г.Алматы',
            'address_agency_request_for_arrest_copy' => 'г.Алматы, ул.Толе би д.155',
            'agency_request_for_deposit' => 'НАО ГК «Правительство для граждан» по г.Алматы',
            'agency_request_for_tech_id' => 'НАО ГК «Правительство для граждан» по г.Алматы',
            'address_agency_request_for_tech_id' => 'г.Алматы, пр.Абая 111',
            'agency_request_for_arrest_copy' => 'НАО ГК «Правительство для граждан» по г.Алматы',
            'address_agency_request_for_deposit' => 'г.Алматы, ул.Толе би 155',
            'agency_request_for' => 'НАО ГК «Правительство для граждан» по г.Алматы',
            'address_agency_request_for' => 'г. Алматы, ул.Толе би 155',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
