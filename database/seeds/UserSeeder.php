<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'role' => 'admin',
            'organization_id' => 1,
            'fullname' => 'Сакен Шерматов',
            'login' => 'boss',
            'email' => 'sake_007@mail.ru',
            'phone' => '810710301087',
            'status' => '1',
            'password' => bcrypt('123123'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
