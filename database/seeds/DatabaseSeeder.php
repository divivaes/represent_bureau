<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(OrganizationSeeder::class);
        $this->call(UserSeeder::class);
    }
}
