<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   if(!auth()->user()) {
       return redirect()->route('login');
   } else {
       return redirect()->route('home');
   }
});

Auth::routes();

Route::group(['prefix' => 'app', 'middleware' => ['auth']], function() {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/users/profile', 'UserController@profile')->name('users.profile');

    Route::resource('papers', 'PaperController');

    Route::group(['prefix' => '/papers/'], function() {
        Route::get('{paper}/choice', 'PaperController@choice')->name('papers.choice');
        Route::get('{paper}/joined', 'PaperController@joined')->name('papers.joined');
        Route::get('{paper}/types', 'PaperController@types')->name('papers.types');
        Route::get('{paper}/types/{type}/prepare', 'PaperController@prepare')->name('papers.prepare');
        Route::post('{paper}/types/{type}/prepare', 'PaperController@prepareForPrint')->name('papers.prepare_for_print');
        Route::get('{join}/invoice', 'PaperController@invoice')->name('papers.invoice');
        Route::get('{join}/invoice/edit-attr', 'PaperController@editAttributes')->name('papers.edit-attr');
        Route::post('{join}/invoice/update-attr', 'PaperController@updateAttributes')->name('papers.update-attr');
    });

    Route::group([], function() {

        Route::resource('organizations', 'OrganizationController');

        Route::resource('categories', 'CategoryController');
        Route::group(['prefix' => 'categories/joiners/'], function () {
            Route::get('list', 'CategoryController@joinerList')->name('categories.joiners.list');
            Route::get('create', 'CategoryController@joinerCreate')->name('categories.joiners.create');
            Route::get('{category}/edit', 'CategoryController@joinerEdit')->name('categories.joiners.edit');
            Route::post('store', 'CategoryController@joinerStore')->name('categories.joiners.store');
            Route::put('{category}/update', 'CategoryController@joinerUpdate')->name('categories.joiners.update');
            Route::delete('{category}/destroy', 'CategoryController@joinerDestroy')->name('categories.joiners.destroy');
        });

        Route::resource('users', 'UserController');
        Route::get('/users/{user}/change', 'UserController@showEditPasswordForm')->name('users.change_password_form');
        Route::post('/users/{user}/change', 'UserController@changePassword')->name('users.change_password');

        Route::group(['prefix' => 'paper-types/'], function () {
            Route::get('', 'PaperTypeController@index')->name('paper_types.index');
            Route::get('create.', 'PaperTypeController@create')->name('paper_types.create');
            Route::get('{paperType}/show', 'PaperTypeController@show')->name('paper_types.show');
            Route::get('{paperType}/edit', 'PaperTypeController@edit')->name('paper_types.edit');
            Route::post('store/', 'PaperTypeController@store')->name('paper_types.store');
            Route::put('{paperType}/update/', 'PaperTypeController@update')->name('paper_types.update');
            Route::delete('{paperType}/destroy', 'PaperTypeController@destroy')->name('paper_types.destroy');
        });

        Route::group(['prefix' => 'paper-type-attributes'], function () {
            Route::get('', 'PaperTypeAttributeController@index')->name('paper_type_attributes.index');
            Route::get('{paperTypeAttribute}/edit', 'PaperTypeAttributeController@edit')->name('paper_type_attributes.edit');
            Route::post('store/', 'PaperTypeAttributeController@store')->name('paper_type_attributes.store');
            Route::put('{paperTypeAttribute}/update/', 'PaperTypeAttributeController@update')->name('paper_type_attributes.update');
            Route::delete('{paperTypeAttribute}/destroy', 'PaperTypeAttributeController@destroy')->name('paper_type_attributes.destroy');
        });

        Route::get('/joiner/edit/{category}/form', 'CategoryPaperTypeController@edit')->name('joiner.edit_data');
        Route::post('/joiner/edit/{category}/form/save', 'CategoryPaperTypeController@update')->name('joiner.update_data');
        Route::delete('/joiner/{category}/delete', 'CategoryPaperTypeController@delete')->name('joiner.delete');

    });

    Route::group(['prefix' => 'settings', 'middleware' => 'creator'], function () {
        Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::get('/templates', 'TemplateController@index')->name('templates.index');
        Route::get('/templates/edit/{template}', 'TemplateController@edit')->name('templates.edit');
        Route::put('/templates/update/{template}', 'TemplateController@update')->name('templates.update');
    });
});
