<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }

    public function boot(): void
    {
        $this->registerPermissions();
    }

    private function registerPermissions(): void
    {
        // TODO Добавить остальные модули гейтов
        Gate::define('create-edit-delete-types', function (User $user) {
            return $user->isCreator();
        });
        Gate::define('list-show-types', function (User $user) {
            return $user->isCreator() || $user->isAdmin();
        });

        Gate::define('create-delete-organizations', function (User $user) {
            return $user->isCreator();
        });
        Gate::define('list-show-organizations', function (User $user) {
            return $user->isCreator() || $user->isAdmin();
        });
    }

}
