<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->isAdmin()) {
            return redirect()->route('papers.index')->with('warning', 'У Вас нет прав посетить данную страницу');
        }
        return $next($request);
    }
}
