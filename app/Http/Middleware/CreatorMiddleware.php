<?php

namespace App\Http\Middleware;

use Closure;

class CreatorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->isCreator()) {
            return redirect()->route('papers.index')->with('warning', 'У Вас нет прав посетить данную страницу');
        }
        return $next($request);
    }
}
