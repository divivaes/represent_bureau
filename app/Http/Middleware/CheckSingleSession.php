<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckSingleSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $previous_session = Auth::User()->session_id;
        if ($previous_session && $previous_session !== \Session::getId()) {

            Session::getHandler()->destroy($previous_session);
            Auth::logout();
            return redirect()->to('login')->with('warning', 'Данный аккаунт активен! Подождите пока пользователь не завершит сессию.');

        } else {
            $request->session()->regenerate();
            Auth::user()->session_id = Session::getId();

            Auth::user()->save();
        }


        return $next($request);

    }
}
