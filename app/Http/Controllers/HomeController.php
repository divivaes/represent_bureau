<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Paper;
use App\Models\PaperType;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $categories = Category::query()->count();
        $papers = Paper::query()->orderBy('created_at', 'desc')->get();
        $types = PaperType::query()->count();
        $users = User::query()->count();

        return view('home.home', [
            'types' => $types,
            'users' => $users,
            'papers' => $papers,
            'categories' => $categories
        ]);
    }
}
