<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\PaperJoin;
use App\Models\PaperJoinAttribute;
use App\Models\PaperType;
use Illuminate\Http\Request;
use App\Http\Requests\PaperRequest;
use App\Models\Paper as Model;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class PaperController extends Controller
{

    public function index(): View
    {
        return view('papers.index', [
            'items' => $q = Model::query()->orderBy('created_at', 'desc')->get()
        ]);
    }

    public function create(): View
    {
        return view('papers.create');
    }

    public function store(PaperRequest $request): RedirectResponse
    {
        $request['user_id'] = auth()->user()->id;
        $request['organization_id'] = auth()->user()->organization->id;
        $request['common_constitutor'] = json_encode($request['common_constitutor'], JSON_UNESCAPED_UNICODE);
        $request['common_constitutor_address'] = json_encode($request['common_constitutor_address'], JSON_UNESCAPED_UNICODE);
//        $request['common_constitutor'] = str_replace('"', "", json_encode($request['common_constitutor']));

        $paper = Model::create($request->all());
        if ($request->input('commit') == 1) {
            return redirect()->to(route('papers.index'))->with('success', 'Документ успешно добавлен');
        } else {
            return redirect()->to(route('papers.edit', $paper));
        }
    }

    public function edit(Model $paper)
    {
        return view('papers.edit', [
            'model' => $paper
        ]);
    }

    public function update(Request $request, Model $paper): RedirectResponse
    {
        $request['user_id'] = auth()->user()->id;
        $request['organization_id'] = auth()->user()->organization->id;
        $request['common_constitutor'] = json_encode($request['common_constitutor'], JSON_UNESCAPED_UNICODE);
        $request['common_constitutor_address'] = json_encode($request['common_constitutor_address'], JSON_UNESCAPED_UNICODE);

        $paper->update($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route('papers.index'))->with('success', 'Документ успешно обновлен');
        } else {
            return redirect()->to(route('papers.edit', $paper));
        }
    }


    public function destroy(Model $paper)
    {
        $paper->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('papers.index'));
        }
    }

    public function choice(Model $paper)
    {
        return view('papers.choice', [
            'model' => $paper->load('joinsWithoutAttributes'),
            'types' => PaperType::query()->orderBy('name', 'asc')->count()
        ]);
    }

    public function joined(Model $paper)
    {
        // TODO
        // Добавить табы категориев к заполненным шаблонам
        return view('papers.joined', [
            'model' => $paper->load('joinsWithPaperType')
        ]);
    }

    public function types(Model $paper)
    {
        $model = $paper->load('joinsWithoutAttributes');
        $typeIds = array();
        $rows = [];

        if ($model->joinsWithoutAttributes()->count() == 0) {
            $rows = Category::query()->orderBy('name', 'asc')->with('paperTypes')->get();
        } else {
            foreach ($model->joinsWithoutAttributes as $joiner) {
                array_push($typeIds, $joiner->paper_type_id);
            }

            $categories = Category::query()->orderBy('name', 'asc')->with('paperTypes')->get();
            foreach ($categories as $category) {
                foreach ($category->paperTypes as $index => $paperType) {
                    if(in_array($paperType->id, $typeIds)) {
                        unset($category->paperTypes[$index]);
                    }
                }
            }
            $rows = $categories;
        }

        return view('papers.types', [
            'model' => $model,
            'rows' => $rows
        ]);
    }

    public function prepare(Model $paper, PaperType $type)
    {
        $paperList = PaperType::query()->orderBy('index', 'asc')->get();
        $typeNames = array();

        if ($type->dependencies != null) {

            $typeIds = explode(",", $type->dependencies);

            if ($paper->joinsWithPaperType->count() >= 1) {
                // TODO нужно вытащить ID незаполненных шаблонов из $typeIds и $paper->joinsWithPaperType
                // пробежать по списку всех шаблонов и заполнить массив $typeNames с названиями недостающих шаблонов
                $existsIds = array();
                foreach ($paper->joinsWithPaperType as $join) {
                    array_push($existsIds, $join->paperType->id);
                }

                $notExistsIds = array_diff($typeIds, $existsIds);

                if (!empty($notExistsIds)) {
                    foreach ($paperList as $paperName) {
                        foreach ($notExistsIds as $typeId) {
                            if ($typeId == $paperName->id) {
                                array_push($typeNames, $paperName->paper_name);
                            }
                        }
                    }

                    return redirect()->to(route('papers.types', $paper))->with('warning', 'Данный шаблон зависим от значении других шаблонов. Пожалуйста заполните эти шаблоны: "' . implode(", ", $typeNames) . '"');
                } else {
                    return view('papers.prepare', [
                        'model' => $paper,
                        'type' => $type->load('paperTypeAttributes'),
                    ]);
                }
            } else {
                foreach ($paperList as $key => $paperName) {
                    foreach ($typeIds as $idx => $typeId) {
                        if ($typeId == $paperName->id) {
                            array_push($typeNames, $paperName->paper_name);
                        }
                    }
                }

                return redirect()->to(route('papers.types', $paper))->with('warning', 'Данный шаблон зависим от значении других шаблонов. Пожалуйста заполните эти шаблоны: "' . implode(", ", $typeNames) . '"');
            }
        } else {
            return view('papers.prepare', [
                'model' => $paper,
                'type' => $type->load('paperTypeAttributes'),
            ]);
        }
    }

    public function prepareForPrint(Request $request, Model $paper, PaperType $type)
    {
        if ($type->injections != null) {
            $injection_ids = explode(',', $type->injections);
            $joins = [];

            $join = new PaperJoin();
            $join->paper_id = $paper->id;
            $join->paper_type_id = $type->id;
            $join->save();

            array_push($joins, $join);

            foreach ($injection_ids as $injection_id) {
                $temp = PaperJoin::create([
                    'paper_id' => $paper->id,
                    'paper_type_id' => $injection_id
                ]);

                array_push($joins, $temp);
            }

            if ($request->attr_name_value != null) {
                foreach ($request->attr_name_value as $key => $attr) {
                    foreach ($joins as $row) {
                        PaperJoinAttribute::create([
                            'paper_join_id' => $row->id,
                            'paper_type_attribute_id' => $key,
                            'attribute_value' => $attr
                        ]);
                    }
                }
            }

            return redirect()->route('papers.invoice', $join);
        } else {
            $join = new PaperJoin();
            $join->paper_id = $paper->id;
            $join->paper_type_id = $type->id;
            $join->save();

            if ($request->attr_name_value != null) {
                foreach ($request->attr_name_value as $idx => $value) {
                    $joinAttribute = new PaperJoinAttribute();
                    $joinAttribute->paper_join_id = $join->id;
                    $joinAttribute->attribute_id = $idx;
                    $joinAttribute->attribute_value = $value;
                    $joinAttribute->save();
                }
            }
            return redirect()->route('papers.invoice', $join);
        }
    }

    public function invoice(PaperJoin $join)
    {
        $invoice = $join->load(['paper', 'paperType', 'attributes']);
        $attributes = $join->attributes->load('paperTypeAttribute');
        $company = $join->paper->load(['company']);

        $previousJoins = $join->paper->load('joins');
        $typeIds = array();
        $temp = array();
        $previous = array();

        if ($invoice->paperType->dependencies != null) {
            $typeIds = explode(",", $invoice->paperType->dependencies);
            if ($previousJoins->joins->count() == count($typeIds)) {
                foreach ($previousJoins as $item) {
                    if ($join->paper_type_id != $item->paper_type_id) {
                        array_push($temp, $item);
                    }
                }
                $previous = array_combine($typeIds, $temp);
            } elseif ($previousJoins->joins->count() > count($typeIds)) {
                foreach ($previousJoins->joins as $item) {
                    if (in_array($item->paper_type_id, $typeIds)) {
                        array_push($temp, $item);
                    }
                }
                $previous = array_combine($typeIds, $temp);
            }
        } else {
            $previous = $attributes;
        }

        return view('papers.invoice', [
            'invoice' => $invoice,
            'attributes' => $attributes,
            'company' => $company,
            'previous' => $previous
        ]);
    }

    public function editAttributes(PaperJoin $join)
    {
        return view('papers.invoice-edit', [
            'invoice' => $join->load(['attributes']),
            'attributes' => $join->attributes->load('paperTypeAttribute')
        ]);
    }

    public function updateAttributes(PaperJoin $join, Request $request)
    {
        $changedAttributes = [];
        array_push($changedAttributes, $request->request->all());
        unset($changedAttributes[0]['_token']);

        $collections = PaperJoinAttribute::where('paper_join_id', $join->id)->get();


        foreach ($changedAttributes[0] as $index => $attr) {
            foreach ($collections as $collection) {
                if ($collection->id == $index) {
                    $collection->attribute_value = $attr;
                    $collection->save();
                }
            }

        }

        return redirect()->route('papers.invoice', $join);
    }
}
