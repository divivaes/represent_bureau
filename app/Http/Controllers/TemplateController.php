<?php

namespace App\Http\Controllers;

use App\Models\PaperType;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function index()
    {
        $q = PaperType::query()->orderByDesc('created_at')->get();

        return view($this->controllerName() . '.index', [
            'items' => $q
        ]);
    }

    public function edit(PaperType $template)
    {
        return view($this->controllerName() . '.edit', [
            'model' => $template
        ]);
    }

    public function update(Request $request, PaperType $template)
    {
        $template->update($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route($this->controllerName() . '.index'))->with('success', 'Документ успешно обновлен');
        } else {
            return redirect()->to(route($this->controllerName() . '.edit', $template));
        }
    }
}
