<?php

namespace App\Http\Controllers;

use App\Models\PaperTypeAttribute as Model;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\JsonResponse;

class PaperTypeAttributeController extends Controller
{
    public function index(): View
    {
        return view('paper_type_attributes.index', [
            'items' => $q = Model::query()->with('paperType')->orderBy('created_at', 'desc')->get()
        ]);
    }

    public function edit(Model $paperTypeAttribute)
    {
        return view( 'paper_type_attributes.edit', [
            'model' => $paperTypeAttribute
        ]);
    }

    public function update(Request $request, Model $paperTypeAttribute): RedirectResponse
    {
        $request['paper_type_id'] = $paperTypeAttribute->paper_type_id;
        $paperTypeAttribute->update($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route('paper_type_attributes.index'))->with('success', 'Изменения успешно сохранились');
        } else {
            return redirect()->to(route('paper_type_attributes.edit', $paperTypeAttribute));
        }
    }
    
    public function destroy(Model $paperTypeAttribute): JsonResponse
    {
        $paperTypeAttribute->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('paper_type_attributes.index'));
        }
    }
}
