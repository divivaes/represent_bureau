<?php

namespace App\Http\Controllers;

use App\Models\PaperType;
use Illuminate\Http\Request;
use App\Models\CategoryPaperType;
use App\Http\Requests\CategoryPaperTypeRequest as JoinerRequest;
use App\Models\Category;
use App\Http\Requests\CategoryRequest as CategoryRequest;

class CategoryPaperTypeController extends Controller
{

    public function index()
    {
        $q = Category::query()->with('joiners')->withCount('joiners')->orderBy('name', 'asc')->get();

        return view($this->controllerName() . '.index', [
            'items' => $q
        ]);
    }

    public function create()
    {
        $cats = Category::query()->orderBy('name', 'asc')->get();
        $types = PaperType::query()->with('paperJoinerIn')->orderBy('paper_name', 'asc')->get();

        return view($this->controllerName() . '.create', [
            'items' => $cats,
            'rows' => $types
        ]);
    }

    public function store(JoinerRequest $request)
    {
        $type_ids = $request->type_id;

        foreach ($type_ids as $id)
        {
            CategoryPaperType::create([
                'category_id' => $request->category_id,
                'paper_type_id' => $id
            ]);
        }

        return redirect()->to(route($this->controllerName() . '.index'))->with('success', 'Связь успешно установлена');
    }

    public function edit($category)
    {
        $category = Category::findOrFail($category);
        $cats = Category::query()->orderBy('name', 'asc')->get();
        $types = PaperType::query()->with('paperJoinerIn')->orderBy('paper_name', 'asc')->get();

        return view($this->controllerName() . '.edit', [
            'model' => $category->load('joiners'),
            'items' => $cats,
            'rows' => $types
        ]);
    }

    public function update(Request $request, $category)
    {
        CategoryPaperType::where('category_id', $category)->delete();
        $type_ids = $request->type_id;

        foreach ($type_ids as $id)
        {
            CategoryPaperType::create([
                'category_id' => $category,
                'paper_type_id' => $id
            ]);
        }

        return redirect()->to(route($this->controllerName() . '.index'))->with('info', 'Связь успешно обновлена');
    }

    public function delete($category)
    {
        CategoryPaperType::where('category_id', $category)->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route($this->controllerName() . '.index'));
        }
    }

}
