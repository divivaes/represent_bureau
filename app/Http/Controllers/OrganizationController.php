<?php

namespace App\Http\Controllers;

//use App\Http\Requests\OrganizationRequest as Request;
use Illuminate\Http\Request as Request;
use App\Models\Organization as Model;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class OrganizationController extends Controller
{
    public function __construct()
    {
//        $this->middleware('creator')->except([
//            'index',
//            'edit',
//            'show'
//        ]);
    }

    public function index(): View
    {
        return view('organizations.index', [
            'items' => $q = Model::query()->orderBy('created_at', 'desc')->get()
        ]);
    }

    public function create(): View
    {
        $model = new Model();

        return view('organizations.create', [
            'model' => $model
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $organization = Model::create($request->all());
        if ($request->input('commit') == 1) {
            return redirect()->to(route('organizations.index'))->with('info', 'Организация успешно добавлена');
        } else {
            return redirect()->to(route('organizations.edit', $organization));
        }
    }

    public function show(Model $organization): View
    {
        return view( 'organizations.show', [
            'model' => $organization
        ]);
    }

    public function edit(Model $organization): View
    {
        return view('organizations.edit', [
            'model' => $organization
        ]);
    }

    public function update(Request $request, Model $organization): RedirectResponse
    {
        $organization->city = $request->city;
        $organization->city_kaz = $request->city_kaz;
        $organization->address_kaz = $request->address_kaz;
        $organization->address_agency_request_for_tech_id = $request->address_agency_request_for_tech_id;
        $organization->update($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route('organizations.index'))->with('info', 'Данные про организацию ' . $organization->name . '  успешно обновлены');
        } else {
            return redirect()->to(route('organizations.edit', $organization));
        }
    }

}
