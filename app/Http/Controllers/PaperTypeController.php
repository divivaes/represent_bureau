<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Models\PaperTypeAttribute;
use App\OrganizationPaperType;
use Illuminate\Http\Request;
use App\Models\PaperType as Model;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\JsonResponse;

class PaperTypeController extends Controller
{
    public function __construct()
    {
//        $this->middleware('creator')->except([
//            'index',
//            'show'
//        ]);
    }

    public function index(): View
    {
        $items = Model::query()->orderBy('index', 'asc')->withCount('paperTypeAttributes')->get();

        return view('paper_types.index', compact('items'));
    }

    public function create(): View
    {
        $paperTypes = Model::query()->orderBy('index', 'asc')->get();
        $organizations = Organization::query()->orderBy('name', 'asc')->get();

        return view('paper_types.create', compact('paperTypes', 'organizations'));
    }

    public function show(Model $paperType): View
    {
        return view( 'paper_types.show', [
            'model' => $paperType->load('paperTypeAttributes')
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        if ($request['dependencies'] != null) {
            $request['dependencies'] = implode(",", $request['dependencies']);    
        }

        if ($request['injections'] != null) {
            $request['injections'] = implode(",", $request['injections']);
        }
    
        $paperType = Model::create($request->all());

        if (!is_null($request->organization_ids)) {
            foreach ($request->organization_ids as $organization_id) {
                OrganizationPaperType::create([
                    'organization_id' => $organization_id,
                    'paper_type_id' => $paperType->id
                ]);
            }
        }

        $attr_usage = array();
        if($request->attr_name != null && $request->attr_type != null) {
            foreach ($request->attr_name as $name) {
                array_push($attr_usage, Str::slug($name));
            }
            $arr = $this->array_zip_combine(['attr_name', 'attr_usage', 'attr_type','attr_format'], $request->attr_name, $attr_usage, $request->attr_type, $request->attr_format);
            foreach ($arr as $value)
            {
                $attribute = new PaperTypeAttribute();
                $attribute->paper_type_id = $paperType->id;
                $attribute->attr_name = $value['attr_name'];
                $attribute->attr_usage = $value['attr_usage'];
                $attribute->attr_type = $value['attr_type'];
                $attribute->attr_format = $value['attr_format'];
                $attribute->save();
            }
        }

        if ($request->input('commit') == 1) {
            return redirect()->to(route('paper_types.index'))->with('success', 'Новый тип документов успешно добавлен');
        } else {
            return redirect()->to(route('paper_types.edit', $paperType));
        }

    }

    public function edit(Model $paperType): View
    {
        if (!is_null($paperType->dependencies)) {
            $dependency_ids = $paperType->dependencies;
            $dependency_ids = explode(',', $dependency_ids);
            $paperType->dependencies = $dependency_ids;
        }

        if (!is_null($paperType->injections)) {
            $injection_ids = $paperType->injections;
            $injection_ids = explode(',', $injection_ids);
            $paperType->injection = $injection_ids;
        }

        $paperTypes = Model::query()->where('id', '!=', $paperType->id)->orderBy('index', 'asc')->get();
        $organizations = Organization::query()->orderBy('name', 'asc')->get();

        return view('paper_types.edit', [
            'model' => $paperType->load(['paperTypeAttributes', 'organization']),
            'organizations' => $organizations,
            'paperTypes' => $paperTypes
        ]);
    }

    public function update(Request $request, Model $paperType): RedirectResponse
    {
        if ($request['dependencies'] != null) {
            $request['dependencies'] = implode(",", $request['dependencies']);
        }

        if ($request['injections'] != null) {
            $request['injections'] = implode(",", $request['injections']);
        }

        $paperType->update($request->all());

        $attr_usage = array();
        foreach ($request->attr_name as $name) {
            array_push($attr_usage, Str::slug($name));
        }

        $arr = $this->array_zip_combine(['attr_name', 'attr_usage', 'attr_type','attr_format'], $request->attr_name, $attr_usage, $request->attr_type, $request->attr_format);

        PaperTypeAttribute::query()->where('paper_type_id', $paperType->id)->delete();

        foreach ($arr as $item) {
            PaperTypeAttribute::create([
                'paper_type_id' => $paperType->id,
                'attr_name' => $item['attr_name'],
                'attr_usage' => $item['attr_usage'],
                'attr_format' => $item['attr_format'],
                'attr_type' => $item['attr_type'],
            ]);
        }

        if ($request->input('commit') == 1) {
            return redirect()->to(route('paper_types.index'))->with('success', 'Изменения успешно сохранились');
        } else {
            return redirect()->to(route('paper_types.edit', $paperType));
        }
    }


    public function destroy(Model $paperType): JsonResponse
    {
        $paperType->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('paper_types.index'));
        }
    }

    function array_zip_combine(array $keys, ...$arrs)
    {
        return array_map(function (...$values) use ($keys) {
            return array_combine($keys, $values);
        }, ...$arrs);
    }


}
