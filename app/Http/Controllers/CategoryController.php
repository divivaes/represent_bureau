<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Requests\CategoryPaperTypeRequest;
use App\Models\Category as Model;
use App\Models\CategoryPaperType;
use App\Models\PaperType;

class CategoryController extends Controller
{
    public function index()
    {
        $q = Model::query()->orderBy('name', 'asc')->get();

        return view('categories.index', [
            'items' => $q
        ]);
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(CategoryRequest $request)
    {
        $category = Model::create($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route('categories.index'))->with('success', 'Категория успешно добавлена');
        } else {
            return redirect()->to(route('categories.edit', $category));
        }
    }

    public function edit(Model $category)
    {
        return view('categories.edit', [
            'model' => $category
        ]);
    }

    public function update(CategoryRequest $request, Model $category)
    {
        $category->update($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route('categories.index'))->with('info', 'Категория успешно обновлена');
        } else {
            return redirect()->to(route('categories.edit', $category));
        }
    }

    public function destroy(Model $category)
    {
        $category->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('categories.index'));
        }
    }

    public function joinerList()
    {
        $items = Model::query()->orderBy('name', 'asc')->with('paperTypes')->get();

        return view('category_paper_types.index', compact('items'));
    }

    public function joinerCreate()
    {
        $categories = Model::query()->orderBy('name', 'asc')->get();
        $paperTypes = PaperType::query()->with('category')->orderBy('index', 'asc')->get();

        return view('category_paper_types.create', compact('categories', 'paperTypes'));
    }

    public function joinerStore(CategoryPaperTypeRequest $request)
    {
        foreach ($request->paper_type_ids as $paper_type_id) {
            CategoryPaperType::create([
                'paper_type_id' => $paper_type_id,
                'category_id' => $request->category_id
            ]);
        }

        return redirect()->to(route('categories.joiners.list'))->with('success', 'Связь успешно установлена');
    }

    public function joinerEdit(Model $category)
    {
        $paperTypes = PaperType::query()->with('category')->orderBy('index', 'asc')->get();

        return view('category_paper_types.edit', compact('category', 'paperTypes'));
    }

    public function joinerUpdate(CategoryPaperTypeRequest $request, Model $category)
    {
        $category->paperTypes()->sync($request->paper_type_ids);

        return redirect()->to(route('categories.joiners.list'))->with('success', 'Связь успешно обновлена');
    }

    public function joinerDestroy(Model $category)
    {
        CategoryPaperType::query()->where('category_id', $category->id)->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('categories.joiners.list'));
        }
    }
}
