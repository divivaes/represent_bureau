<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserRequest as Request;
use App\Http\Requests\User\UserChangePasswordRequest as EditRequest;
use App\Models\User as Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UserController extends Controller
{
    public function index(): View
    {
        return view('users.index', [
            'items' => $q = Model::query()->orderBy('created_at', 'desc')->get()
        ]);
    }
    
    public function create()
    {
        $model = new Model();
        $roles = Model::rolesList();

        return view('users.create', [
            'model' => $model,
            'items' => $roles
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $request['password'] = Hash::make($request->password);
        $user = Model::create($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route('users.index'))->with('success', 'Пользователь успешно добавлен');
        } else {
            return redirect()->to(route('users.edit', $user));
        }
    }

    public function show(Model $user): View
    {
        return view( 'users.show', [
            'model' => $user
        ]);
    }

    public function edit(Model $user): View
    {
        return view('users.edit', [
            'model' => $user,
            'items' => $roles = Model::rolesList()
        ]);
    }

    public function update(Request $request, Model $user): RedirectResponse
    {
        $user->update($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route('users.index'))->with('success', 'Пользователь успешно обновлен');
        } else {
            return redirect()->to(route('users.edit', $user));
        }
    }

    public function destroy(Model $user): JsonResponse
    {
        $user->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('users.index'));
        }
    }

    public function showEditPasswordForm(Model $user): View
    {
        return view('users.change_password', [
            'model' => $user
        ]);
    }

    public function changePassword(EditRequest $request, Model $user): RedirectResponse
    {
        $request['password'] = Hash::make($request->password);
        $user->update($request->all());

        if ($request->input('commit') == 1) {
            return redirect()->to(route('users.index'))->with('info', 'Пароль успешно обновлен');
        } else {
            return redirect()->to(route('users.show', $user))->with('info', 'Пароль успешно обновлен');
        }
    }
}
