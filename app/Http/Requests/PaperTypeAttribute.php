<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaperTypeAttribute extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'type_id' => 'required|integer',
            'attr_name' => 'required|string|max:255',
            'attr_usage' => 'string|max:255',
            'attr_type' => 'string|max:255',
            'attr_format' => 'string|max:255'
        ];
    }

    public function attributes(): array
    {
        return [
            'type_id' => 'ID наследовательного документа',
            'attr_name' => 'Наименование атрибута для документа',
            'attr_usage' => 'Наименование атрибута для взаимодействия на сервере',
            'attr_type' => 'Тип атрибута',
            'attr_format' => 'Формат атрибута'
        ];
    }
}
