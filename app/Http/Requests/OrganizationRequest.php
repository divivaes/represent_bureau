<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            => 'string|max:255',
            'inflexive_name'  => 'string|max:255',
            'city'            => 'string|max:500',
            'city_kaz'        => 'string|max:500',
            'address'         => 'string|max:500',
            'address_kaz'     => 'string|max:500',
            'email'           => 'string|max:255',
            'phone'           => 'string|max:255',
            'mobile_phone'    => 'string|max:255',
            'license_date'    => 'string|max:255',
            'license_number'  => 'string|max:255',
            'owner_fullname'  => 'string|max:255',
            'owner_id'        => 'string|max:255',
            'bank_requisites' => 'max:1000',
            'mrp'             => 'integer|max:10',
            'agency_request_for' => 'string|max:500',
            'address_agency_request_for' => 'string|max:500',
            'agency_request_for_tech_id' => 'string|max:500',
            'address_agency_request_for_tech_id' => 'string|max:500',
            'agency_request_for_deposit' => 'string|max:500',
            'address_agency_request_for_deposit' => 'string|max:500',
            'agency_request_for_arrest_copy' => 'string|max:500',
            'address_agency_request_for_arrest_copy' => 'string|max:500',
            'agency_for_arrest_car' => 'string|max:500',
            'seller_company_name' => 'string|max:255',
            'seller_site' => 'string|max:255',
            'agency_for_arrest_spec_car' => 'string|max:500',
            'agency_address_for_request_passport' => 'string|max:500',
            'agency_address_for_request_gov_act' => 'string|max:500'
        ];
    }

    public function attributes()
    {
        return [
            'name'            => 'Название компании|Директора',
            'inflexive_name'  => 'Склонямое название компании|Директора',
            'city'            => 'Город организации на русском языке',
            'city_kaz'        => 'Город организации на казахском языке',
            'address'         => 'Адрес организации на русском языке',
            'address_kaz'     => 'Адрес организации на казахском языке',
            'email'           => 'Электронный адрес',
            'phone'           => 'Домашний телефон',
            'mobile_phone'    => 'Рабочий телефон',
            'bank_requisites' => 'Банковские реквизиты',
            'mrp'             => 'МРП',
            'license_date'    => 'Дата лицензии',
            'license_number'  => 'Номер лицензии',
            'owner_fullname'  => 'ФИО ЧСИ',
            'owner_id'        => 'ИИН ЧСИ',
            'agency_request_for' => 'Орган, откуда запрашиваешь правдоки',
            'address_agency_request_for' => 'Адрес органа, откуда запрашиваешь правдоки',
            'agency_request_for_tech_id' => 'Орган откуда запрашиваешь технический паспорт и гос.акт',
            'address_agency_request_for_tech_id' => 'Адрес, органа откуда запрашиваешь технический паспорт и гос.акт',
            'agency_request_for_deposit' => 'Орган, откуда заправшиваешь договор залога',
            'address_agency_request_for_deposit' => 'Адрес органа, откуда заправшиваешь договор залога',
            'agency_request_for_arrest_copy' => 'Орган, откуда запрашиваешь копии постановлении о наложении ареста',
            'address_agency_request_for_arrest_copy' => 'Адрес органа, откуда запрашиваешь копии постановлении о наложении ареста',
            'agency_for_arrest_car' => 'Орган, куда отправляют арест на авто',
            'seller_company_name' => 'Наименование органа, который проводит торги',
            'seller_site' => 'Сайт электронной торговой площадки',
            'agency_for_arrest_spec_car' => 'Орган, куда отправляют арест на спецтехнику',
            'agency_address_for_request_passport' => 'Адрес, откуда запрашиваешь технический паспорт',
            'agency_address_for_request_gov_act' => 'Орган, откуда запрашиваешь гос.акт'
        ];
    }
}
