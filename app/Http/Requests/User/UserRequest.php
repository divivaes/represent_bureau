<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public $name_array = 'users';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'role' => 'string|max:50',
            'organization_id' => 'integer|max:3',
            'fullname' => 'string|max:255',
            'login' => 'string|max:255',
            'email' => 'string|max:255',
            'phone' => 'string|max:15',
            'password' => 'confirmed|string|max:255',
            'status' => 'integer|max:2'
        ];
    }
}
