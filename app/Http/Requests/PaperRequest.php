<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaperRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'integer|max:10',
            'organization_id' => 'integer|max:10',
            'claimant_type' => 'string|max:255',
            'debtor_type' => 'string|max:255',
            'debtor_firstname' => 'string|max:255|nullable',
            'debtor_lastname' => 'string|max:255|nullable',
            'debtor_surname' => 'string|max:255|nullable',
            'debtor_company_name' => 'string|max:255|nullable',
            'common_constitutor' => 'JSON|nullable',
            'common_constitutor_address' => 'string|max:255|nullable',
            'debtor_mentor_firstname' => 'string|max:255|nullable',
            'debtor_mentor_lastname' => 'string|max:255|nullable',
            'debtor_mentor_surname' => 'string|max:255|nullable',
            'debtor_mentor_address' => 'string|max:255|nullable',
            'debtor_email' => 'string|max:255|nullable',
            'debtor_phone' => 'string|max:25|nullable',
            'debtor_id' => 'string|max:255|nullable',
            'debtor_address' => 'string|max:255|nullable',
            'debtor_perflist_address' => 'string|max:255|nullable',
            'debtor_workplace' => 'string|max:255|nullable',
            'debtor_workplace_address' => 'string|max:255|nullable',
            'claimant_firstname' => 'string|max:255|nullable',
            'claimant_lastname' => 'string|max:255|nullable',
            'claimant_surname' => 'string|max:255|nullable',
            'claimant_company_name' => 'string|max:255|nullable',
            'claimant_mentor_firstname' => 'string|max:255|nullable',
            'claimant_mentor_lastname' => 'string|max:255|nullable',
            'claimant_mentor_surname' => 'string|max:255|nullable',
            'claimant_mentor_address' => 'string|max:255|nullable',
            'claimant_id' => 'string|max:255|nullable',
            'claimant_email' => 'string|max:255|nullable',
            'claimant_phone' => 'string|max:255|nullable',
            'claimant_address' => 'string|max:255|nullable',
            'claimant_sum' => 'string|max:30|nullable',
            'claimant_content' => 'string|max:255|nullable',
            'exec_doc_number' => 'string|max:255|required',
            'exec_doc_name' => 'string|max:255|required',
            'exec_doc_owner' => 'string|max:255|required',
            'exec_doc_created_at' => 'string|max:255|required',
            'enf_proc_number' => 'string|max:255|nullable',
            'enf_proc_date' => 'string|max:255|nullable'
        ];
    }

    public function attributes()
    {
        return [
            'organization_id' => 'ID организации',
            'user_id' => 'ID пользователя',
            'claimant_type' => 'Тип лица взыскателя',
            'debtor_type' => 'Тип лица должника',
            'debtor_firstname' => 'Имя должника',
            'debtor_lastname' => 'Фамилия должника',
            'debtor_surname' => 'Отчество должника',
            'debtor_company_name' => 'Наименование ТОО или ИП должника',
            'common_constitutor' => 'Учредитель',
            'common_constitutor_address' => 'Адрес Учредителя',
            'debtor_mentor_firstname' => 'Имя руководителя должника',
            'debtor_mentor_lastname' => 'Фамилия руководителя должника',
            'debtor_mentor_surname' => 'Отчество руководителя должника',
            'debtor_mentor_address' => 'Адрес руководителя должника',
            'debtor_email' => 'Почта должника',
            'debtor_phone' => 'Телефон должника',
            'debtor_id' => 'ИИН или БИН должника',
            'debtor_address' => 'Адрес должника по месту регистрации',
            'debtor_perflist_address' => 'Адрес должника по исполнительному листу',
            'debtor_workplace' => 'Наименование организации, где работает должник',
            'debtor_workplace_address' => 'Адрес организации, где работает должник',
            'claimant_firstname' => 'Имя взыскателя',
            'claimant_lastname' => 'Фамилия взыскателя',
            'claimant_surname' => 'Отчество взыскателя',
            'claimant_company_name' => 'Наимпенование ТОО или ИП взыскателя',
            'claimant_mentor_firstname' => 'Имя руководителя взыскателя',
            'claimant_mentor_lastname' => 'Фамилия руководителя взыскателя',
            'claimant_mentor_surname' => 'Отчество руководителя взыскателя',
            'claimant_mentor_address' => 'Адрес руководителя взыскателя',
            'claimant_email' => 'Почта взыскателя',
            'claimant_phone' => 'Номер взыскателя',
            'claimant_address' => 'Адрес взыскателя',
            'claimant_sum' => 'Сумма взыскания всего',
            'claimant_content' => 'Сущность взыскания',
            'exec_doc_number' => 'Номер исполнительного документа',
            'exec_doc_name' => 'Наименование исполнительного документа',
            'exec_doc_owner' => 'Kем выдан исполнительный документ',
            'exec_doc_created_at' => 'Дата выдачи исполнительного документа',
            'enf_proc_number' => 'Номер исполнительного производства',
            'enf_proc_date' => 'Дата возбуждения исполнительного производства'
        ];
    }
}
