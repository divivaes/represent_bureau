<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaperTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'paper_id' => 'integer',
            'paper_name' => 'required|string|max:255',
            'paper_inflexive_name' => 'string|max:255',
            'paper_tpl_path' => 'string|max:255',
            'status' => 'required|integer|max:1',
            'index' => 'required|integer',
            'dependencies' => 'string',
            'injections' => 'string'
        ];
    }

    public function attributes(): array
    {
        return [
            'paper_id' => 'ID документа',
            'paper_name' => 'Название наследовательного документа',
            'paper_inflexive_name' => 'Склонямое название документа',
            'paper_tpl_path' => 'Путь к шаблону документа',
            'status' => 'Статус',
            'index' => 'Порядок шаблона',
            'dependencies' => 'ID других шаблонов от которых зависит данный шаблон',
            'injections' => 'ID шаблонов которые заполняются при старте'
        ];
    }
}
