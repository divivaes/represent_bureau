<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryPaperTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'integer|max:15',
            'paper_type_id' => 'array|max:15'
        ];
    }

    public function attributes(): array
    {
        return [
            'category_id' => 'ID категории',
            'paper_type_id' => 'ID шаблона',
        ];
    }
}
