<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $category_id
 * @property integer $paper_type_id
 * @property string $created_at
 * @property string $updated_at
 * @property Category $categories
 * @property PaperType $paperType
 */
class CategoryPaperType extends Model
{
    protected $fillable = ['category_id', 'paper_type_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function paperType()
    {
        return $this->belongsTo(PaperType::class);
    }

    public function paperTypeOld()
    {
        return $this->belongsTo(PaperType::class)->withCount('paperTypeAttributes')->with('join');
    }

    public function paperTypeWithAttributesCountAndJoins()
    {
        return $this->belongsTo(PaperType::class)->withCount('paperTypeAttributes')->with('join');
    }
}
