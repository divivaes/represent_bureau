<?php

namespace App;

use App\Models\Boot\InsertGeoDetails;
use App\Models\Organization;
use App\Models\PaperType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationPaperType extends Model
{
    use InsertGeoDetails;
    use SoftDeletes;

    protected $fillable = [
        'paper_type_id',
        'organization_id'
    ];

    public function paperType()
    {
        return $this->belongsTo(PaperType::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
