<?php

namespace App\Models;

use App\Models\Boot\InsertGeoDetails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property CategoryPaperType[] $joiners
 */
class Category extends Model
{
    use SoftDeletes;
    use InsertGeoDetails;

    protected $fillable = ['name'];

    public function joiners()
    {
        return $this->hasMany(CategoryPaperType::class)->with('paperType');
    }

    public function joinedPapers()
    {
        return $this->hasMany(CategoryPaperType::class)->with('paperType');
    }

    public function paperTypes()
    {
        return $this->belongsToMany(PaperType::class, 'category_paper_types', 'category_id', 'paper_type_id')->where('status', '=', 1)->orderBy('index', 'asc')->withTimestamps();
    }
}
