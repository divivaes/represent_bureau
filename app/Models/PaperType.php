<?php

namespace App\Models;

use App\Models\Boot\InsertGeoDetails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $paper_id
 * @property string $paper_name
 * @property string $inflexive_paper_name
 * @property string $paper_tpl_path
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property Paper $paper
 * @property PaperTypeAttribute[] $typeAttributes
 */
class PaperType extends Model
{
    use SoftDeletes;
    use InsertGeoDetails;

    protected $keyType = 'integer';
    protected $fillable = [
        'paper_id', 'paper_name', 'inflexive_paper_name', 'paper_tpl_path', 'status', 'index', 'dependencies', 'injections'
    ];

    public function paper()
    {
        return $this->belongsToMany(Paper::class, 'paper_joins', 'paper_type_id', 'paper_id');
    }

    public function organization()
    {
        return $this->belongsToMany(Organization::class, 'organization_paper_types', 'paper_type_id', 'organization_id');
    }

    public function join()
    {
        return $this->hasMany(PaperJoin::class, 'paper_type_id');
    }

    public function paperTypeAttributes()
    {
        return $this->hasMany(PaperTypeAttribute::class, 'paper_type_id');
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'category_paper_types', 'paper_type_id', 'category_id')->withTimestamps();
    }
}
