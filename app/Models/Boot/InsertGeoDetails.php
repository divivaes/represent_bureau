<?php

namespace App\Models\Boot;

use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;
use PulkitJalan\GeoIP\GeoIP;

trait InsertGeoDetails
{
    public static function bootInsertGeoDetails()
    {
        static::saving(function ($model) {
            $config = [
                'driver' => 'maxmind',
                'maxmind' => [
                    'database' => storage_path() . '/GeoLite2-City.mmdb',
                ],
            ];
            $geoip = new GeoIP($config);
            $agent = new Agent();

            if (!$model->submitter_ip) {
                $model->submitter_ip = $geoip->getIp();
            }

            if (!$model->submitter_city) {
                $model->submitter_city = $geoip->get('city');
            }

            if (!$model->submitter_country) {
                $model->submitter_country = $geoip->get('country');
            }

            if (!$model->submitter_browser) {
                $model->submitter_browser = $agent->browser();
            }

            if (!$model->submitter_browser) {
                $model->submitter_browser = $agent->browser();
            }

            if (!$model->submitter_platform) {
                $model->submitter_platform = $agent->platform();
            }

            if (!$model->submitter_agent) {
                $model->submitter_agent = $agent->getUserAgent();
            }
        });
    }
}
