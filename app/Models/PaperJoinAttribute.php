<?php

namespace App\Models;

use App\Models\Boot\InsertGeoDetails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $paper_join_id
 * @property integer $attribute_id
 * @property string $attribute_value
 * @property string $created_at
 * @property string $updated_at
 * @property PaperTypeAttribute $typeAttribute
 * @property PaperJoin $paperJoin
 */
class PaperJoinAttribute extends Model
{
    use SoftDeletes;
    use InsertGeoDetails;

    protected $keyType = 'integer';
    protected $fillable = ['paper_join_id', 'paper_type_attribute_id', 'attribute_value', 'attribute_name'];

    public function paperTypeAttribute()
    {
        return $this->belongsTo(PaperTypeAttribute::class);
    }

    public function paperJoin()
    {
        return $this->belongsTo(PaperJoin::class);
    }
}
