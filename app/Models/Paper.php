<?php

namespace App\Models;

use App\Http\Petrovich;
use App\Models\Boot\InsertGeoDetails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $debtor_type
 * @property integer $claimant_type
 * @property string $debtor_name
 * @property string $debtor_email
 * @property string $debtor_phone
 * @property string $debtor_iin
 * @property string $debtor_address
 * @property string $debtor_perflist_address
 * @property string $debtor_workplace
 * @property string $debtor_workplace_address
 * @property string $claimant_name
 * @property string $claimant_email
 * @property string $claimant_address
 * @property string $claimant_sum
 * @property string $exec_doc_number
 * @property string $exec_doc_name
 * @property string $exec_doc_owner
 * @property string $exec_doc_created_at
 * @property string $enf_proc_number
 * @property string $enf_proc_date
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Paper extends Model
{
    use SoftDeletes;
    use InsertGeoDetails;

    protected $keyType = 'integer';
    protected $fillable = ['user_id', 'organization_id', 'claimant_type', 'debtor_type', 'debtor_firstname', 'debtor_lastname', 'debtor_surname', 'debtor_company_name', 'common_constitutor', 'common_constitutor_address','debtor_mentor_firstname', 'debtor_mentor_lastname', 'debtor_mentor_surname', 'debtor_mentor_address', 'debtor_email', 'debtor_phone', 'debtor_id', 'debtor_address', 'debtor_perflist_address', 'debtor_workplace', 'debtor_workplace_address', 'claimant_firstname', 'claimant_lastname', 'claimant_surname', 'claimant_company_name', 'claimant_mentor_firstname', 'claimant_mentor_lastname', 'claimant_mentor_surname', 'claimant_mentor_address', 'claimant_id', 'claimant_email', 'claimant_phone', 'claimant_address', 'claimant_sum', 'claimant_content', 'exec_doc_number', 'exec_doc_name', 'exec_doc_owner', 'exec_doc_created_at', 'enf_proc_number', 'enf_proc_date'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function company()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }

    public function joins()
    {
        return $this->hasMany(PaperJoin::class, 'paper_id')->orderBy('paper_type_id','asc')->with('attributes');
    }

    public function joinsWithoutAttributes()
    {
        return $this->hasMany(PaperJoin::class, 'paper_id');
    }

    public function joinsWithPaperType()
    {
        return $this->hasMany(PaperJoin::class, 'paper_id')->with('paperType');
    }

    /**
     * Функции для возврата ФИО должника
     * @return mixed
     */
    public function getDebtorCompanyName()
    {
        return $this->debtor_company_name;
    }


    public function getDebtorFullName()
    {
        if($this->debtor_surname == null) {
            return $this->debtor_firstname . ' ' . $this->debtor_lastname;
        } else {
            return $this->debtor_lastname . ' ' . $this->debtor_firstname . ' ' . $this->debtor_surname;
        }

    }

    public function getDebtorMentorFullName()
    {
        if($this->debtor_mentor_surname == null) {
            return $this->debtor_mentor_firstname . ' ' . $this->debtor_mentor_lastname;
        } else {
            return $this->debtor_mentor_firstname . ' ' . $this->debtor_mentor_surname;
        }

    }

    public function getDebtorCompanyNameOrFullName()
    {
        if($this->debtor_type == 1) {
            return $this->getDebtorCompanyName();
        } else {
            return $this->getDebtorFullName();
        }
    }

    /**
     * Функция возвращает ФИО в дательнем падеже или название компании
     */
    public function getInclinedDativeDebtorName()
    {
        if ($this->debtor_type == 1) {
            return $this->getDebtorCompanyName();
        } else {
            return $this->getInflexiveDebtorFullName();
        }
    }

    public function getDebtorOnlyCompanyNameOrFullName()
    {
        if ($this->debtor_type == 1) {
            return $this->getDebtorCompanyName();
        } else {
            return $this->getInflexiveDebtorFullName();
        }
    }

    /**
     * Функция возвращает ФИО в творительном падеже или название компании
     */
    public function getInclinedInstrumentalDebtorName()
    {
        if ($this->debtor_type == 1) {
            return $this->getDebtorCompanyName();
        } else {
            return $this->getInstrumentalDebtorFullName();
        }
    }

    /**
     * Функция возвращает ФИО в родительном падеже или название компании
     */
    public function getInclinedGenitiveDebtorName()
    {
        if ($this->debtor_type == 1) {
            return $this->getDebtorCompanyName();
        } else {
            return $this->getGenitiveDebtorFullName() . ', ' . $this->debtor_address;
        }
    }


    /**
     * Функции для возврата ФИО взыскателя
     * @return string
     */
    public function getClaimantFullName()
    {
        if($this->claimant_surname == null) {
            return $this->claimant_firstname . ' ' . $this->claimant_lastname;
        } else {
            return $this->claimant_lastname . ' ' . $this->claimant_firstname . ' ' . $this->claimant_surname;
        }
    }

    public function getClaimantMentorFullName()
    {
        if($this->claimant_mentor_surname == null) {
            return $this->claimant_mentor_firstname . ' ' . $this->claimant_mentor_lastname;
        } else {
            return $this->claimant_mentor_firstname . ' ' . $this->claimant_mentor_lastname . ' ' . $this->claimant_mentor_surname;
        }
    }

    /**
     * Функция для склонения ФИО руководителя должника по дательному падежу
     * @return int
     */
    public function getInflexiveDebtorFullName()
    {
        $gender = $this->detectGender($this->debtor_surname);
        $petrovich = new Petrovich($gender);

        $firstname = $petrovich->firstname($this->debtor_firstname, Petrovich::CASE_DATIVE);
        $lastname = $petrovich->lastname($this->debtor_lastname, Petrovich::CASE_DATIVE);
        $surname = $petrovich->middlename($this->debtor_surname, Petrovich::CASE_DATIVE);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }

    public function getClaimantCompanyName()
    {
        return $this->claimant_company_name;
    }

    public function getClaimantCompanyNameOrFullName()
    {
        if($this->claimant_type == 1) {
            return $this->getClaimantCompanyName();
        } else  {
            return $this->getClaimantFullName();
        }
    }


    /**
     * Функция для определения пола
     * @param $str
     * @return int
     */
    public function detectGender($str)
    {
        $gender = new Petrovich();
        $str = $gender->detectGender($str);
        return $str;
    }

    /**
     * Функция для склонения ФИО должника по дательному падежу
     * @return int
     */
    public function getInflexiveDebtorMentorFullName()
    {
        $gender = $this->detectGender($this->debtor_mentor_surname);
        $petrovich = new Petrovich($gender);

        $firstname = $petrovich->firstname($this->debtor_mentor_firstname, Petrovich::CASE_DATIVE);
        $lastname = $petrovich->lastname($this->debtor_mentor_lastname, Petrovich::CASE_DATIVE);
        $surname = $petrovich->middlename($this->debtor_mentor_surname, Petrovich::CASE_DATIVE);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }

    /**
     * Функция для склонения ФИО должника по творительному падежу
     * @return int
     */
    public function getInstrumentalDebtorFullName()
    {
        $gender = $this->detectGender($this->debtor_surname);
        $petrovich = new Petrovich($gender);

        $firstname = $petrovich->firstname($this->debtor_firstname, Petrovich::CASE_INSTRUMENTAL);
        $lastname = $petrovich->lastname($this->debtor_lastname, Petrovich::CASE_INSTRUMENTAL);
        $surname = $petrovich->middlename($this->debtor_surname, Petrovich::CASE_INSTRUMENTAL);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }

    /**
     * Функция для склонения ФИО должника по родительному падежу
     * @return int
     */
    public function getGenitiveDebtorFullName()
    {
        $gender = $this->detectGender($this->debtor_surname);
        $petrovich = new Petrovich($gender);

        $firstname = $petrovich->firstname($this->debtor_firstname, Petrovich::CASE_GENITIVE);
        $lastname = $petrovich->lastname($this->debtor_lastname, Petrovich::CASE_GENITIVE);
        $surname = $petrovich->middlename($this->debtor_surname, Petrovich::CASE_GENITIVE);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }

    /**
     * Функция для склонения ФИО взыскателя
     * @return int
     */
    public function getInflexiveClaimantFullName()
    {
        $gender = $this->detectGender($this->claimant_surname);
        $petrovich = new Petrovich($gender);

        $firstname = $petrovich->firstname($this->claimant_firstname, Petrovich::CASE_DATIVE);
        $lastname = $petrovich->lastname($this->claimant_lastname, Petrovich::CASE_DATIVE);
        $surname = $petrovich->middlename($this->claimant_surname, Petrovich::CASE_DATIVE);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }

    /**
     * Расчет общей суммы деятельности ЧСИ
     * @param $sum
     * @param $mrp
     * @return int
     */
    public function getFees($sum, $mrp)
    {
        if($sum < $mrp * 60) {
            return round(($sum * 25) / 100);
        } elseif($sum <= $mrp * 300) {
            return round(($sum * 20) / 100);
        } elseif($sum <= $mrp * 1000) {
            return round(($sum * 15) / 100);
        } elseif($sum <= $mrp * 5000) {
            return round(($sum * 10) / 100);
        } elseif($sum <= $mrp * 10000) {
            return round(($sum * 8) / 100);
        } elseif($sum <= $mrp * 20000) {
            return round(($sum * 5) / 100);
        } elseif($sum < $mrp * 20001) {
            return round(($sum * 3) / 100);
        }
    }
    /**
     *
     * @param $value
     * @param $mrp
     * @return int
     */
    public function getPercent($value, $mrp)
    {
        if($value<$mrp*60) {
            return 25;
        } elseif($value<=$mrp*300) {
            return 20;
        } elseif($value<=$mrp*1000) {
            return 15;
        } elseif($value<=$mrp*5000) {
            return 10;
        } elseif($value<=$mrp*10000) {
            return 8;
        } elseif($value<=$mrp*20000) {
            return 5;
        } elseif($value>$mrp*20001) {
            return 3;
        }
    }

    public function getMoney($value, $mrp)
    {
        if($value<$mrp*60) {
            return round(($value*25)/100);
        } elseif($value<=$mrp*300) {
            return round(($value*20)/100);
        } elseif($value<=$mrp*1000) {
            return round(($value*15)/100);
        } elseif($value<=$mrp*5000) {
            return round(($value*10)/100);
        } elseif($value<=$mrp*10000) {
            return round(($value*8)/100);
        } elseif($value<=$mrp*20000) {
            return round(($value*5)/100);
        } elseif($value>$mrp*20001) {
            return round(($value*3)/100);
        }
    }

    /**
     * Склонение наименование исполнительного документа
     * @return string
     */
    public function declensionDocName($text, $index){
        $urlXml = "https://ws3.morpher.ru/russian/declension?s=".urlencode($text);
        $result = @simplexml_load_file($urlXml);
        if($result){
            $arrData = array();
            foreach ($result as $one) {
               $arrData[] = (string) $one;
            }
            return $arrData[$index];
        }
        return false;
    }

    public function getGenitiveConstitutorFullName()
    {
        $arr = explode(" ", json_decode($this->common_constitutor));

        $gender = $this->detectGender($arr[2]);
        $petrovich = new Petrovich($gender);

        $lastname = $petrovich->lastname($arr[0], Petrovich::CASE_GENITIVE);
        $firstname = $petrovich->firstname($arr[1], Petrovich::CASE_GENITIVE);
        $surname = $petrovich->middlename($arr[2], Petrovich::CASE_GENITIVE);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }

    public function getDativeConstitutorFullName()
    {
        $arr = explode(" ", json_decode($this->common_constitutor));

        $gender = $this->detectGender($arr[2]);
        $petrovich = new Petrovich($gender);

        $lastname = $petrovich->lastname($arr[0], Petrovich::CASE_DATIVE);
        $firstname = $petrovich->firstname($arr[1], Petrovich::CASE_DATIVE);
        $surname = $petrovich->middlename($arr[2], Petrovich::CASE_DATIVE);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }

    public function getInstrumentalConstitutorFullName()
    {
        $arr = explode(" ", json_decode($this->common_constitutor));

        $gender = $this->detectGender($arr[2]);
        $petrovich = new Petrovich($gender);

        $lastname = $petrovich->lastname($arr[0], Petrovich::CASE_INSTRUMENTAL);
        $firstname = $petrovich->firstname($arr[1], Petrovich::CASE_INSTRUMENTAL);
        $surname = $petrovich->middlename($arr[2], Petrovich::CASE_INSTRUMENTAL);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }
}
