<?php

namespace App\Models;

use App\Http\Petrovich;
use App\Models\Boot\InsertGeoDetails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $inflexive_name
 * @property string $slug
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $bank_requisites
 * @property string $mrp
 * @property string $created_at
 * @property string $updated_at
 * @property User[] $users
 */
class Organization extends Model
{
    use SoftDeletes;
    use InsertGeoDetails;

    protected $fillable = ['name', 'inflexive_name', 'slug', 'address', 'email', 'phone', 'bank_requisites', 'mrp', 'agency_request_for', 'address_agency_request_for', 'agency_request_for_tech_id', 'agency_request_for_deposit', 'address_agency_request_for_deposit', 'agency_request_for_arrest_copy', 'address_agency_request_for_arrest_copy', 'owner_id', 'owner_fullname', 'license_date', 'license_number', 'mobile_phone', 'agency_for_arrest_car', 'seller_company_name', 'seller_site', 'agency_for_arrest_spec_car', 'agency_address_for_request_passport', 'agency_address_for_request_gov_act'
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function docs()
    {
        return $this->hasMany(Paper::class, 'organization_id');
    }

    public function getMRP()
    {
        $attribute = $this->getAttribute('mrp');

        return $attribute;
    }

    /**
     * Функция для определения пола
     * @param $str
     * @return int
     */
    public function detectGender($str)
    {
        $gender = new Petrovich();
        $str = $gender->detectGender($str);
        return $str;
    }

    /**
     * Функция для склонения ФИО ЧСИ по дательному падежу
     * @return int
     */
    public function getGenitiveOwnerFullName()
    {
        $arr = explode(" ", $this->owner_fullname);

        $gender = $this->detectGender($arr[2]);
        $petrovich = new Petrovich($gender);
        $surname = $petrovich->middlename($arr[2], Petrovich::CASE_DATIVE);
        $firstname = $petrovich->firstname($arr[1], Petrovich::CASE_DATIVE);
        $lastname = $petrovich->lastname($arr[0], Petrovich::CASE_DATIVE);

        return $lastname . ' ' . $firstname . ' ' . $surname;
    }
}
