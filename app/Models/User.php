<?php

namespace App\Models;

use App\Models\Boot\InsertGeoDetails;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements AuthenticatableContract
{
    use Notifiable;
    use SoftDeletes;
    use InsertGeoDetails;

    public const ROLE_USER = 'user';
    public const ROLE_CLIENT = 'client';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_CREATOR = 'creator';

    protected $fillable = [
        'fullname', 'email', 'password', 'login', 'phone', 'status', 'organization_id', 'role', 'session_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function papers()
    {
        return $this->hasMany(Paper::class, 'user_id')->with('joins', 'organization');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }

    public static function rolesList(): array
    {
        return [
            self::ROLE_USER => 'Работник',
            self::ROLE_CLIENT => 'Клиент',
            self::ROLE_ADMIN => 'Администратор',
        ];
    }

    public function isCreator(): bool
    {
        return $this->role === self::ROLE_CREATOR;
    }

    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function isClient(): bool
    {
        return $this->role === self::ROLE_CLIENT;
    }

    public function isUser(): bool
    {
        return $this->role === self::ROLE_USER;
    }
}
