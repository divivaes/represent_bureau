<?php

namespace App\Models;

use App\Models\Boot\InsertGeoDetails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $paper_id
 * @property integer $paper_type_id
 * @property string $created_at
 * @property string $updated_at
 * @property Paper $paper
 * @property PaperType $paperType
 */
class PaperJoin extends Model
{
    use SoftDeletes;
    use InsertGeoDetails;

    protected $keyType = 'integer';
    protected $fillable = [
        'paper_id', 'paper_type_id'
    ];

    public function paper()
    {
        return $this->belongsTo(Paper::class);
    }

    public function paperType()
    {
        return $this->belongsTo(PaperType::class);
    }

    public function paperTypeWithAttributeCount()
    {
        return $this->belongsTo(PaperType::class)->withCount('paperTypeAttributes');
    }

    public function paperTypeWithAttributes()
    {
        return $this->belongsTo(PaperType::class)->with('paperTypeAttributes');
    }

    public function paperTypeWithAttributesAndCategory()
    {
        return $this->belongsTo(PaperType::class)->with('paperJoinerIn')->withCount('paperTypeAttributes');
    }

    public function attributes()
    {
        return $this->hasMany(PaperJoinAttribute::class, 'paper_join_id');
    }
}
