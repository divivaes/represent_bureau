<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $type_id
 * @property string $attr_name
 * @property string $attr_usage
 * @property string $attr_type
 * @property string $attr_format
 * @property string $created_at
 * @property string $updated_at
 * @property PaperType $paperType
 */
class PaperTypeAttribute extends Model
{
    use SoftDeletes;

    protected $keyType = 'integer';
    protected $fillable = ['paper_type_id', 'attr_name', 'attr_usage', 'attr_type', 'attr_format', 'created_at', 'updated_at'];

    public function paperType()
    {
        return $this->belongsTo(PaperType::class);
    }

    public function convertToReadable()
    {
        if($this->attr_type == 'string')
            return 'Короткий текст';
        if($this->attr_type == 'text')
            return 'Длинный текст';
        if($this->attr_type == 'date')
            return 'Дата';
    }
}
