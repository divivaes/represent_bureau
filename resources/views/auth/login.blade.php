@extends('layouts.app')

@section('login')

<div class="login-box">

    <div class="login-logo">
        <a href="javascript:void(0);"><b>АИС ЧСИ</b></a>
    </div>
    @include('layouts.messages')
    <div class="login-box-body">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Электронная почта" value="{{ old('email') }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Пароль">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }} name="remember"> Запомнить
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                </div>
            </div>
        </form>
    </div>
    @if ($errors->has('email'))
        <span class="alert alert-warning" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    @if ($errors->has('password'))
        <span class="alert alert-warning" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
</div>
@endsection
